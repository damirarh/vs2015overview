﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Math;

namespace NewInCs6
{
    [TestClass]
    public class NewFeatures
    {
        #region 1 - await in catch and finally

        #region Helpers
        private class Resource
        {
            public static async Task<Resource> OpenAsync()
            {
                return await Task.FromResult(new Resource());
            }

            public async Task LogAsync(Exception e)
            {
                await Task.Yield();
            }

            public async Task CloseAsync()
            {
                await Task.Yield();
            }
        }
        #endregion

        [TestMethod]
        public async Task AwaitInCatchAndFinally()
        {
            Resource res = null;
            try
            {
                res = await Resource.OpenAsync();
            }
            catch (Exception e)
            {
                await res.LogAsync(e);
            }
            finally
            {
                if (res != null)
                {
                    await res.CloseAsync();
                }
            }
        }

        #endregion

        #region 2 - nameof() expression

        private void CheckParameter(object param)
        {
            if (param == null)
            {
                throw new ArgumentNullException(nameof(param));
            }
        }

        [TestMethod]
        public void NameofExpression()
        {
            CheckParameter(null);
        }

        #endregion

        #region 3 - auto-property initializers

        private class Customer
        {
            public string First { get; set; } = "Mads";
            public string Last { get; } = "Torgersen";
            public string Title { get; }

            public Customer()
            {
                Title = "Mr";
            }
        }

        [TestMethod]
        public void AutoPropertyInitializers()
        {
            var customer = new Customer();
        }

        #endregion

        #region 4 - expression bodied function members

        private class Person
        {
            public string Name { get; set; }
            public string Surname { get; set; }

            public string FullName => Name + " " + Surname;

            public override string ToString() => "Person: " + FullName;

            public Person(string name, string surname)
            {
                Name = name;
                Surname = surname;
            }
        }

        [TestMethod]
        public void ExpressionBodiedFunctionMembers()
        {
            var person = new Person("Anders", "Hejlsberg");
        }

        #endregion

        #region 5 - null-conditional operator

        [TestMethod]
        public void NullConditionalOperator()
        {
            string name = null;
            var length = name?.Length ?? 0;
        }

        #endregion

        #region 6 - exception filter

        [TestMethod]
        public void ExceptionFilter()
        {
            try
            {
                var command = new SqlCommand();
                command.ExecuteNonQuery();
            }
            catch (SqlException e) if (e.Class >= 17)
            {
                Trace.TraceError(e.ToString());
            }
        }

        #endregion

        #region 7 - string interpolation

        [TestMethod]
        public void StringInterpolation()
        {
            Console.WriteLine("Current time is \{DateTime.Now : T}.");
        }

        #endregion

        #region 8 - using static

        [TestMethod]
        public void UsingStatic()
        {
            var sqrt = Sqrt(2);
        }

        #endregion
    }
}
